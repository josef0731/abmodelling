import pandas as pd
import numpy as np
import itertools
import os
import matplotlib.pyplot as plt
import seaborn as sns
import get_mutations as mut

%matplotlib inline
sns.set_theme()
sns.set_context('paper')
sns.set_palette('crest')

colours = sns.color_palette(palette = 'crest', n_colors = 6)

patient_id_list = [
    'YF189',
    'YF191',
    'YF192',
    'YF199',
    'YF200',
    'YF203'
]

charged_aminoacids = ['R', 'H', 'K', 'D', 'E']
poscharged_aminoacids = ['R', 'H', 'K']
negcharged_aminoacids = ['D', 'E']
uncharged_aminoacids = ['Q', 'N', 'S', 'T', 'Y', 'C']
nonpolar_aminoacids = ['A', 'I', 'L', 'M', 'F', 'V', 'P', 'G', 'W']


def create_MutPosition_plots(patient_id_list):
    '''Creates histograms of MutPosition occurances for each patient_id and
    across all patient_ids given the patient_id_list arg'''

    # creates dir for plots if it doesn't already exists
    plot_dir_path = os.getcwd() + os.sep + 'results' + os.sep + \
        'mutation_data' + os.sep + 'MutPosition_plots'
    if not os.path.exists(plot_dir_path):
        os.makedirs(plot_dir_path)

    # creates list to store individual patient_id dfs in for aggregate stats
    df_list = []

    # loops through patient_ids, appends dataframes to df_list and creates
    # histograms of MutPosition distributions
    for patient_id in patient_id_list:
        df = mut.create_mut_dataframe(patient_id)
        df_list.append(df)
        max_MutPosition = df.MutPosition.max()
        fig, ax = plt.subplots(figsize=(7,5))
        sns.histplot(
            ax = ax,
            data = df,
            x = 'MutPosition',
            kde = True,
            bins = max_MutPosition,
            color = colours[4])
        ax.set_xlabel('Mutation Position')
        ax.set_ylabel('Counts')
        ax.set_xlim(left=0, right=120)
        ax.set_ylim(bottom=0, top=10000)
        ax.set_title(patient_id + ' - Distribution of Mutation Posistions')
        plot_path = plot_dir_path + os.sep + \
            patient_id + '_MutPosition_hist.pdf'
        fig.savefig(plot_path)

    # concatenates df_list to a single dataframe and generates average
    # MutPosition histogram
    df_all = pd.concat(df_list)
    max_MutPosition = df.MutPosition.max()
    fig, ax = plt.subplots(figsize=(7,5))
    sns.histplot(
        ax = ax,
        data = df_all,
        x = 'MutPosition',
        kde = True,
        bins = max_MutPosition,
        color = colours[4])
    ax.set_xlabel('Mutation Position')
    ax.set_ylabel('Counts')
    ax.set_xlim(left=0, right=120)
    ax.set_ylim(bottom=0, top=50000)
    ax.set_title('All Patients - Distribution of Mutation Posistions')
    plot_path = plot_dir_path + os.sep + 'AllPatients_MutPosition_hist.pdf'
    fig.savefig(plot_path)


def create_Region_plots_raw(patient_id_list):
    '''Creates histograms of mutation Region occurances for each patient_id and
    across all patient_ids given the patient_id_list arg'''

    # creates dir for plots if it doesn't already exists
    plot_dir_path = os.getcwd() + os.sep + 'results' + os.sep + \
                'mutation_data' + os.sep + 'Region_plots' + os.sep + 'raw'
    if not os.path.exists(plot_dir_path):
        os.makedirs(plot_dir_path)

    # creates list to store individual patient_id dfs in for aggregate stats
    df_list = []

    # loops through patient_ids, appends dataframes to df_list and creates
    # 'histograms' of Region distributions
    for patient_id in patient_id_list:
        df = mut.create_mut_dataframe(patient_id)
        df_list.append(df)
        fig, ax = plt.subplots(figsize=(7,5))
        sns.countplot(
            ax = ax,
            data = df,
            x = 'Region',
            palette = 'crest')
        ax.set_xlabel('Mutation Region')
        ax.set_ylabel('Counts')
        # ax.set_ylim(bottom=0, top=10000)
        ax.set_title(patient_id + ' - Distribution of Mutations within Regions')
        plot_path = plot_dir_path + os.sep + patient_id + \
                    '_Region_count_raw.pdf'
        fig.savefig(plot_path)

    # concatenates df_list to a single dataframe and generates a single
    # Region distribution 'histogram'
    df_all = pd.concat(df_list)
    fig, ax = plt.subplots(figsize=(7,5))
    sns.countplot(
        ax = ax,
        data = df_all,
        x = 'Region',
        palette = 'crest')
    ax.set_xlabel('Mutation Region')
    ax.set_ylabel('Counts')
    # ax.set_ylim(bottom=0, top=50000)
    ax.set_title('All Patients - Distribution of Mutations within Regions')
    plot_path = plot_dir_path + os.sep + 'AllPatients_Region_count_raw.pdf'
    fig.savefig(plot_path)


def create_Region_plots_prop(patient_id_list):
    '''Creates bar plots of the mutation Region occurances for each patient_id
    and across all patient_ids, proportional to the number of amino acids in the
    region'''

    # creates dir for plots if it doesn't already exists
    plot_dir_path = os.getcwd() + os.sep + 'results' + os.sep + \
        'mutation_data' + os.sep + 'Region_plots' + os.sep + 'prop'
    if not os.path.exists(plot_dir_path):
        os.makedirs(plot_dir_path)

    # creates list to store individual patient_id dfs in for aggregate stats
    df_list = []

    # stores lengths of each region as variables
    FR1_len = 26-0
    FR2_len = 55-38
    FR3_len = 104-65
    CDR1_len = 38-26
    CDR2_len = 65-55
    CDR3_len = 117-104

    # loops through patient_ids, appends dataframes to df_list and creates
    # 'histograms' of Region distributions
    for patient_id in patient_id_list:
        df = mut.create_mut_dataframe(patient_id)
        df_list.append(df)

        df = df.groupby('Region').count()[['SeqID']] \
            .rename({'SeqID': 'Counts'}, axis=1).reset_index()

        df.loc[df.Region == 'FR1', 'Prop_Counts'] = df.Counts / FR1_len
        df.loc[df.Region == 'FR2', 'Prop_Counts'] = df.Counts / FR2_len
        df.loc[df.Region == 'FR3', 'Prop_Counts'] = df.Counts / FR3_len
        df.loc[df.Region == 'CDR1', 'Prop_Counts'] = df.Counts / CDR1_len
        df.loc[df.Region == 'CDR2', 'Prop_Counts'] = df.Counts / CDR2_len
        df.loc[df.Region == 'CDR3', 'Prop_Counts'] = df.Counts / CDR3_len

        Prop_Counts_total = df.Prop_Counts.sum()

        df.loc[df.Region == 'FR1', 'Prop_pct'] = \
            df.Prop_Counts / Prop_Counts_total
        df.loc[df.Region == 'FR2', 'Prop_pct'] = \
            df.Prop_Counts / Prop_Counts_total
        df.loc[df.Region == 'FR3', 'Prop_pct'] = \
            df.Prop_Counts / Prop_Counts_total
        df.loc[df.Region == 'CDR1', 'Prop_pct'] = \
            df.Prop_Counts / Prop_Counts_total
        df.loc[df.Region == 'CDR2', 'Prop_pct'] = \
            df.Prop_Counts / Prop_Counts_total
        df.loc[df.Region == 'CDR3', 'Prop_pct'] = \
            df.Prop_Counts / Prop_Counts_total

        df = df.reindex([3, 0, 4, 1, 5, 2]).reset_index()

        fig, ax = plt.subplots(figsize=(7,5))
        sns.barplot(
            ax = ax,
            x = 'Region',
            y = 'Prop_pct',
            data = df,
            palette = 'crest')
        ax.set_xlabel('Mutation Region')
        ax.set_ylabel('Proportional Percentage of Mutations')
        ax.set_title(patient_id + \
            ' - Proportional Distribution of Mutations within Regions')
        plot_path = plot_dir_path + os.sep + patient_id + \
                    '_Region_count_proportional.pdf'
        fig.savefig(plot_path)

    # concatenates df_list to a single dataframe and generates a single
    # Region distribution 'histogram'
    df_all = pd.concat(df_list)
    df_all = df_all.groupby('Region').count()[['SeqID']] \
        .rename({'SeqID': 'Counts'}, axis=1).reset_index()

    df_all.loc[df_all.Region == 'FR1', 'Prop_Counts'] = \
        df_all.Counts / FR1_len
    df_all.loc[df_all.Region == 'FR2', 'Prop_Counts'] = \
        df_all.Counts / FR2_len
    df_all.loc[df_all.Region == 'FR3', 'Prop_Counts'] = \
        df_all.Counts / FR3_len
    df_all.loc[df_all.Region == 'CDR1', 'Prop_Counts'] = \
        df_all.Counts / CDR1_len
    df_all.loc[df_all.Region == 'CDR2', 'Prop_Counts'] = \
        df_all.Counts / CDR2_len
    df_all.loc[df_all.Region == 'CDR3', 'Prop_Counts'] = \
        df_all.Counts / CDR3_len

    Prop_Counts_total = df_all.Prop_Counts.sum()

    df_all.loc[df_all.Region == 'FR1', 'Prop_pct'] = \
        df_all.Prop_Counts / Prop_Counts_total
    df_all.loc[df_all.Region == 'FR2', 'Prop_pct'] = \
        df_all.Prop_Counts / Prop_Counts_total
    df_all.loc[df_all.Region == 'FR3', 'Prop_pct'] = \
        df_all.Prop_Counts / Prop_Counts_total
    df_all.loc[df_all.Region == 'CDR1', 'Prop_pct'] = \
        df_all.Prop_Counts / Prop_Counts_total
    df_all.loc[df_all.Region == 'CDR2', 'Prop_pct'] = \
        df_all.Prop_Counts / Prop_Counts_total
    df_all.loc[df_all.Region == 'CDR3', 'Prop_pct'] = \
        df_all.Prop_Counts / Prop_Counts_total

    display(df_all)

    df_all = df_all.reindex([3, 0, 4, 1, 5, 2]).reset_index()

    fig, ax = plt.subplots(figsize=(7,5))
    sns.barplot(
        ax = ax,
        x = 'Region',
        y = 'Prop_pct',
        data = df_all,
        palette = 'crest')
    ax.set_xlabel('Mutation Region')
    ax.set_ylabel('Proportional Percentage of Mutations')
    ax.set_title(patient_id + \
        ' - Proportional Distribution of Mutations within Regions')
    plot_path = plot_dir_path + os.sep + \
        'AllPatients_Region_count_proportional.pdf'
    fig.savefig(plot_path)


def draw_heatmap(df):
    df = df[['germline_aa', 'sequence_aa']]

    df[['Germ_IsCharged',
        'Germ_IsPosCharged',
        'Germ_IsNegCharged',
        'Germ_IsUncharged',
        'Germ_IsNonPolar',
        'Seq_IsCharged'
        'Seq_IsPosCharged',
        'Seq_IsNegCharged',
        'Seq_IsUncharged',
        'Seq_IsNonPolar']] = 0

    df.loc[df.germline_aa.isin(charged_aminoacids), \
        'Germ_IsCharged'] = 1
    df.loc[df.germline_aa.isin(poscharged_aminoacids), \
        'Germ_IsPosCharged'] = 1
    df.loc[df.germline_aa.isin(negcharged_aminoacids), \
        'Germ_IsNegCharged'] = 1
    df.loc[df.germline_aa.isin(uncharged_aminoacids), \
        'Germ_IsUncharged'] = 1
    df.loc[df.germline_aa.isin(nonpolar_aminoacids), \
        'Germ_IsNonPolar'] = 1
    df.loc[df.sequence_aa.isin(charged_aminoacids), \
        'Seq_IsCharged'] = 1
    df.loc[df.sequence_aa.isin(poscharged_aminoacids), \
        'Seq_IsPosCharged'] = 1
    df.loc[df.sequence_aa.isin(negcharged_aminoacids), \
        'Seq_IsNegCharged'] = 1
    df.loc[df.sequence_aa.isin(uncharged_aminoacids), \
        'Seq_IsUncharged'] = 1
    df.loc[df.sequence_aa.isin(nonpolar_aminoacids), \
        'Seq_IsNonPolar'] = 1

    df_corr = df.corr()

    df_corr = df_corr.drop([
        'Seq_IsCharged',
        'Germ_IsCharged',
        'Germ_IsPosCharged',
        'Germ_IsNegCharged',
        'Germ_IsNonPolar',
        'Germ_IsUncharged'])
    df_corr = df_corr.drop(columns=[
        'Germ_IsCharged',
        'Seq_IsCharged',
        'Seq_IsPosCharged',
        'Seq_IsNegCharged',
        'Seq_IsNonPolar',
        'Seq_IsUncharged'])
    df_corr = df_corr[[
        'Germ_IsNonPolar',
        'Germ_IsUncharged',
        'Germ_IsNegCharged',
        'Germ_IsPosCharged']]

    return sns.heatmap(df_corr, annot=True, cmap='coolwarm')


def create_mut_heatmaps(patient_id_list):

    # # creates dir for plots if it doesn't already exists
    # plot_dir_path = os.getcwd() + os.sep + 'results' + os.sep + \
    #             'mutation_data' + os.sep + 'Region_plots' + os.sep + 'raw'
    # if not os.path.exists(plot_dir_path):
    #     os.makedirs(plot_dir_path)
    #
    # # creates list to store individual patient_id dfs in for aggregate stats
    # df_CDR1_list = []
    # df_CDR2_list = []
    # df_CDR3_list = []
    # df_FR1_list = []
    # df_FR2_list = []
    # df_FR3_list = []

    # loops through patient_ids, appends dataframes to df_list and creates
    # 'histograms' of Region distributions
    for patient_id in patient_id_list:
        df = mut.create_mut_dataframe(patient_id)

        df = df[['Region', 'germline_aa', 'sequence_aa']]

        df[['Germ_IsCharged',
            'Germ_IsPosCharged',
            'Germ_IsNegCharged',
            'Germ_IsUncharged',
            'Germ_IsNonPolar',
            'Seq_IsCharged',
            'Seq_IsPosCharged',
            'Seq_IsNegCharged',
            'Seq_IsUncharged',
            'Seq_IsNonPolar']] = 0

        df.loc[df.germline_aa.isin(charged_aminoacids), \
            'Germ_IsCharged'] = 1
        df.loc[df.germline_aa.isin(poscharged_aminoacids), \
            'Germ_IsPosCharged'] = 1
        df.loc[df.germline_aa.isin(negcharged_aminoacids), \
            'Germ_IsNegCharged'] = 1
        df.loc[df.germline_aa.isin(uncharged_aminoacids), \
            'Germ_IsUncharged'] = 1
        df.loc[df.germline_aa.isin(nonpolar_aminoacids), \
            'Germ_IsNonPolar'] = 1
        df.loc[df.sequence_aa.isin(charged_aminoacids), \
            'Seq_IsCharged'] = 1
        df.loc[df.sequence_aa.isin(poscharged_aminoacids), \
            'Seq_IsPosCharged'] = 1
        df.loc[df.sequence_aa.isin(negcharged_aminoacids), \
            'Seq_IsNegCharged'] = 1
        df.loc[df.sequence_aa.isin(uncharged_aminoacids), \
            'Seq_IsUncharged'] = 1
        df.loc[df.sequence_aa.isin(nonpolar_aminoacids), \
            'Seq_IsNonPolar'] = 1

        df_corr = df.corr()

        df_corr = df_corr.drop([
            'Seq_IsCharged',
            'Germ_IsCharged',
            'Germ_IsPosCharged',
            'Germ_IsNegCharged',
            'Germ_IsNonPolar',
            'Germ_IsUncharged'])
        df_corr = df_corr.drop(columns=[
            'Germ_IsCharged',
            'Seq_IsCharged',
            'Seq_IsPosCharged',
            'Seq_IsNegCharged',
            'Seq_IsNonPolar',
            'Seq_IsUncharged'])
        df_corr = df_corr[[
            'Germ_IsNonPolar',
            'Germ_IsUncharged',
            'Germ_IsNegCharged',
            'Germ_IsPosCharged']]

        figure = sns.FacetGrid(df, col='Region')
        figure.map(sns.heatmap, data=df_corr, annot=True, cmap='coolwarm')

        figure.savefig(patient_id + 'forarchie.pdf')

        # df_list = []
        # df_names = ['CDR1', 'CDR2', 'CDR3', 'FR1', 'FR2', 'FR3']
        #
        # df_CDR1 = df[df.Region == 'CDR1']
        # df_CDR1_list.append(df_CDR1)
        # df_CDR2 = df[df.Region == 'CDR2']
        # df_CDR2_list.append(df_CDR2)
        # df_CDR3 = df[df.Region == 'CDR3']
        # df_CDR3_list.append(df_CDR3)
        # df_FR1 = df[df.Region == 'FR1']
        # df_FR1_list.append(df_FR1)
        # df_FR2 = df[df.Region == 'FR2']
        # df_FR2_list.append(df_FR2)
        # df_FR3 = df[df.Region == 'FR3']
        # df_FR3_list.append(df_FR3)
        #
        # figure = figsize()
        #
        # counter = 0
        # for dataframe in df_list:
        #
        # df_name = df_names[counter]
        # counter += 1

create_mut_heatmaps(patient_id_list)
