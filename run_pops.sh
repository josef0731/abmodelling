#!/bin/bash 
#######################################
# Run POPS over a list of *.pdb files #
#######################################

# usage: ./run_pops.sh listOfPdb
# where listOfPdb is a file with list of pdb file names (with extension *.pdb), 1 PDB file per line.

while read pdb; do
	echo $pdb
	pdbcode=${pdb%.pdb}
	if [ ! -f `echo $pdbcode`.pops ]; then
		pops --pdb $pdb --residueOut --popsOut `echo $pdbcode`.pops || true		
		rm -rf popsb.out sigma.out
	fi
done < $1
