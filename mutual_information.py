import pandas as pd
from pyitlib import discrete_random_variable as drv

def caclMI(df, columns):
    """
    calculate mutual information between columns.
    based on "Notes on getting started" on pyitlib pypi page (https://pypi.org/project/pyitlib/)
    :arg df: the pandas data frame
    :arg columns: list of column names to be considered
    """
    df = df[columns]
    return drv.information_mutual_normalised(df.T)

def bootstrapDF(df, n_iter = 1000):
    """
    bootstrap sampling (i.e. sampling with replacement) a data frame
    :arg df: the pandas data frame
    :arg n_iter: number of sampling iterations (default: 1000)
    """
    return [ df.sample(n=df.shape[0], replace=True, random_state=i+1) for i in range(n_iter) ]

def getMIdist(df, columns, n_iter=1000):
    """
    get bootstrap MI distribution from a data frame
    :arg df: the pandas data frame
    :arg columns: list of column names to be considered
    :arg n_iter: number of sampling iterations (default: 1000)
    """
    return [ calcMI(d, columns) for d in bootstrapDF(df, n_iter) ]

def getBootstrapCI(stats, level=0.95):
    """
    get boostrapped confidence intervals (CI)
    :arg stats: the list of stats from which CI is taken
    :arg level: 1-alpha for the required CI. (Default: 0.95, i.e. 95% confidence interval)
    """
    if type(level) is not float:
        return "level needs to be a float. Exit."
    if level <= 0 or level >= 1:
        return "level must be between 0 and 1. Exit."
    bound1 = (1 - level) / 2.0
    bound2 = level + bound1
    return np.percentile(stats, [bound1, bound2])
