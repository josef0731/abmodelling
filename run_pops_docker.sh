#!/bin/bash 
#######################################
# Run POPS over a list of *.pdb files #
#######################################

# usage: ./run_pops_docker.sh listOfPdb
# where listOfPdb is a file with list of pdb file names (with extension *.pdb), 1 PDB file per line.

# make sure you have pull the docker
# you may need to change the version (v.3.x.x or similar) below to match the version of docker image you pulled. 

# initiate
docker run --name popsdocker --cidfile test.cid -itd popscomp/popscomp:v3.1.6
cid=$(cat test.cid)

# copy files to docker container
echo "Copying PDB files ..."
while read pdb; do
	echo $pdb
	docker cp $pdb popsdocker:/
done < $1
docker cp run_pops.sh popsdocker:/
docker cp $1 popsdocker:/

# and then run the run_pops.sh inside the docker container
echo "Running POPS ..."
docker exec -it popsdocker /run_pops.sh /$1

# copy files back
echo "Copying POPS output back ..."
while read pdb; do
	echo $pdb
	fn=${pdb%.pdb}
	docker cp popsdocker:/`echo $fn`.pops ./
done < $1

# clean up (remove exited docker containers)
docker stop $cid
docker rm $(docker ps -q -f status=exited)
rm test.cid
