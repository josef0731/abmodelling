import pandas as pd
import numpy as np
import itertools
import os
import xlsxwriter
import clean_repertoire_data as crd


def get_aa_type(aa):
    '''Takes a single character string representing an amino acid as an arg
    and returns the classification of that amino acid'''

    if aa in ['R', 'K', 'D', 'E']:
        return 'Polar Charged'
    elif aa in ['Q', 'N', 'H', 'S', 'T', 'Y', 'C']:
        return 'Polar Uncharged'
    elif aa in ['A', 'I', 'L', 'M', 'F', 'V', 'P', 'G']:
        return 'Non-Polar'


def get_mut_dictionary(patient_id):
    '''Loops through the dataframe created using the 'create_sequence_dataframe'
    function and finds mutations between the germline and sequence amino acid
    code and returns them and their position as a nested dictoinary'''

    # runs 'create_sequence_dataframe' function
    df_sequence = crd.prepare_sequence_dataframe(patient_id)

    # filters 'df_sequence' based on it's 'productive' column
    df_sequence = df_sequence[df_sequence.productive == 'T']

    # removes rows where 'v_sequence_alignment_aa' values contains '*' character
    # to prevent errors when submitting to ABodyBuilder
    df_sequence = df_sequence[
        ~df_sequence.v_sequence_alignment_aa.str.contains('*', regex=False)]

    # runs 'create_repertoire_dataframe'
    df_repertoire = crd.prepare_repertoire_dataframe(patient_id) \
                    [['CloneID', 'CellID', 'SeqID', 'UseAsRef']]

    # filters dataframe for 'CloneID' values using 'get_top_heavy_clones'
    # funtion
    df_top_heavy_clones = df_repertoire[df_repertoire.CloneID \
                            .isin(crd.get_top_heavy_clones(patient_id))]

    # filters dataframe for 'CellID' values using 'CellID' values associated
    # with 'CloneID' values from 'df_top_heavy_clones' and stores all 'SeqID'
    # values (H and L) to list
    df_top_cells = df_repertoire[df_repertoire.CellID \
                    .isin(list(df_top_heavy_clones.CellID))]

    # list of all 'SeqID' values associated with top 'CloneID' values (H and L)
    top_SeqID_list = list(df_top_cells.SeqID)

    # filters 'df_repertoire' using 'top_SeqID_list'
    df_repertoire = df_repertoire[df_repertoire.SeqID.isin(top_SeqID_list)]

    # dataframe of top 'CloneID' values and 'SeqID' value filtered by 'UseAsRef'
    df_top_clones_ref = df_top_cells[df_top_cells.UseAsRef == True]
    df_top_clones_ref = df_top_clones_ref[['CloneID', 'SeqID']]

    # merges 'df_top_clone_ref' and 'df_sequence' to give dataframe of 'CloneID'
    # values associated with 'SeqID' values that are marked with
    # 'UseAsRef' == True
    df_top_clones_ref = df_top_clones_ref \
        .merge(df_sequence, left_on='SeqID', right_on='sequence_id')
    df_top_clones_ref = df_top_clones_ref.rename(columns={
        'v_germline_alignment_aa': 'v_germline_alignment_aa_ref'})
    df_top_clones_ref = df_top_clones_ref[[
        'CloneID',
        'v_germline_alignment_aa_ref']]

    # merges 'df_repertoire' and 'df_sequence'
    df = df_repertoire \
        .merge(df_sequence, left_on='SeqID', right_on='sequence_id')

    # merges 'df' and 'df_top_clones_ref'
    df = df.merge(df_top_clones_ref, left_on='CloneID', right_on='CloneID')
    df = df[[
        'CloneID',
        'SeqID',
        'v_germline_alignment_aa',
        'v_sequence_alignment_aa',
        'v_germline_alignment_aa_ref' ]]

    # loops through each 'SeqID' value in 'df_sequence' and finds differences
    # between 'v_germline_alignment_aa_ref' and 'sequene_alignment_aa' values
    # stores mutation position, germline aa and sequence aa in dictionary
    nested_dict = {}
    for row in df.itertuples():
        nested_dict[row.SeqID] = {}
        position = 1
        for aa in row.v_germline_alignment_aa_ref:
            if aa != 'X':
                germline_aa = aa
                if position <= len(row.v_sequence_alignment_aa):
                    sequence_aa = row.v_sequence_alignment_aa[position-1]
                    if germline_aa != sequence_aa:
                        nested_dict[row.SeqID][position] = {
                                'germline_aa': germline_aa,
                                'sequence_aa': sequence_aa,
                                'CloneID': row.CloneID}
            position += 1

    return nested_dict


def create_mut_dataframe(patient_id):
    '''Takes nested dictionary from 'get_mutation_dictionary' as arg and
    converts to multiindex dataframe'''

    mutation_dictionary = get_mut_dictionary(patient_id)

    # converts nested dictionary to dictionary of tuples for later
    dict_tuples = {(k1, k2):v2 for k1,v1 in mutation_dictionary.items() \
                               for k2,v2 in mutation_dictionary[k1].items()}

    # converts dictionary of tuples to multiindex dataframe
    df = pd.DataFrame([dict_tuples[i] for i in sorted(dict_tuples)],
        index=pd.MultiIndex.from_tuples(
            [i for i in sorted(dict_tuples.keys())]))

    # renames columns
    colnames = ['SeqID', 'MutPosition', 'germline_aa', 'sequence_aa', 'CloneID']
    df = df.reset_index()
    df.columns = colnames

    # adds 'Region' columns using conditions based on IMGT numbering scheme
    conditions = [
        (df.MutPosition > 0) & (df.MutPosition <= 26),
        (df.MutPosition > 26) & (df.MutPosition <= 38),
        (df.MutPosition > 38) & (df.MutPosition <= 55),
        (df.MutPosition > 55) & (df.MutPosition <= 65),
        (df.MutPosition > 65) & (df.MutPosition <= 104),
        (df.MutPosition > 104) & (df.MutPosition <= 117),
        (df.MutPosition > 117),]
    values = [
        'FR1',
        'CDR1',
        'FR2',
        'CDR2',
        'FR3',
        'CDR3',
        'FR4']
    df['Region'] = np.select(conditions, values)

    # adds two columns which define the type of amino acid
    # (polar charged, polar uncharged and non-polar)
    df['germline_aa_type'] = df.germline_aa.apply(lambda x: get_aa_type(x))
    df['sequence_aa_type'] = df.sequence_aa.apply(lambda x: get_aa_type(x))

    # converts dataframe back to MultiIndex DataFrame
    # df = df.set_index(['SeqID', 'MutPosition'])

    return df


def write_mut_dataframe_to_csv(patient_id):
    '''Takes patient_id str as arg and saves dataframe of tuples from
    'create_mutation_dataframe' as arg and writes to .xlxs file in
    results/mutation_data path'''

    mut_dataframe = create_mut_dataframe(patient_id)

    # saves mut_dataframe as .csv file in \results\mutation_data\ dir
    dir_path = os.getcwd() + os.sep + 'results' + os.sep + 'mutation_data'
    file_path = dir_path + os.sep + patient_id + '_mutation_data.csv'
    mut_dataframe.to_csv(file_path, index=False)


# for patient_id in [
#         'YF189',
#         'YF191',
#         'YF192',
#         'YF199',
#         'YF200',
#         'YF203']:
#     write_mut_dataframe_to_csv(patient_id)
