# Modelling Antibodies from single-cell-resolved B-cell repertoires from Yellow Fever vaccination. #

Luke Marques' project.

## To-do

Updated 29-Jan-2021

1. Selection of large sequence clusters to model + justification of the selection criteria

    * Consider only heavy chain clusters.
    * **Filter to remove paired H-L which are nonproductive for either chain.** Use column `productive` (== 'T') and check '*' in `sequence_alignment_aa`, for both H and L.
    * How large are the clusters? - show histograms. Report number, sizes and number of paired H-L of top X% large clusters.
    * Establish clusters which are large ('responsive'?) vs clusters which persists across timepoints ('persistent'). Show large 'persistent' clusters in terms of their number, their sizes and the number of paired H-L for modelling.

2. Submission to ABodyBuilder

    * **If statement to skip paired H-L which are modelled already!**
    
3. Function to parse `sequence_alignment_aa` and `germline_alignment_aa` to get mutations - position, wild-type and mutant AA.   

4. SASA calculation with POPS for your models.

5. Map mutations to structure.


## Contents of this repo

1. submission of paired antibody heavy- and light-chains to ABodyBuilder for modelling.
2. codes for evaluation and comparison of structures [see below].
3. miscellanous codes for analysis.

## Command-line calculation of structure statistics

1. RMSD: Check python package [rmsd](https://pypi.org/project/rmsd/).
2. SASA: Use Fraternali lab package [popscomp](https://github.com/Fraternalilab/POPScomp). install source code of POPSC and POPSR (see the POPScomp README for details).

POPScomp consists of two 'subpackages':

* POPS: a parametrised method to quickly estimate per-atom/per-residue etc. solvent-accessible surface area to a given structure. `POPSC` is the code to do this.
* POPScomp: build upon POPS, annotate interface residues based on the change in SASA when comparing the proteins in complex versus them alone (since those with large delta-SASA should be in the interface). `POPSR` is the current version, which is a R package that calls `POPSC` repeatedly.

Here are the code to call POPS:

1. `run_pops.sh`: process one PDB file at a time and run POPS locally. *This requires local installation of POPSC.*
2. `run_pops_docker.sh`: calls `run_pops.sh` but uses the docker image instead. Please look through the POPScomp github wiki to pull a docker image.

If in trouble for (1) (e.g. trouble in installation in non-UNIX machines) use the docker image.
